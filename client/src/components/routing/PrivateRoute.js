import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, Redirect} from 'react-router-dom';

const Dashboard = ({ component: Component, auth: { isAuthenticated, loading }, ...rest }) => (
    <Route {...rest} render={props => {
      return (
        !isAuthenticated && !loading ? <Redirect to="/login" /> : <Component {...props} />
      );
    }} />
  );

Dashboard.propTypes = {
  auth: PropTypes.object.isRequired
};

export default connect(state => ({
  auth: state.auth
}))(Dashboard);